package server

import (
	"log"
	"net"

	"google.golang.org/grpc"

	"gitlab.com/ekdiaz08/grpc-gateway/pkg/user"
)

func StartGrpc() error {
	server := grpc.NewServer()
	userImp := user.NewHandler()
	user.RegisterUserServiceServer(server, userImp)
	if l, err := net.Listen("tcp", ":8080"); err != nil {
		log.Fatal("error in listening on port :8080 ", err)
	} else {
		if err := server.Serve(l); err != nil {
			log.Fatal("unable to start server", err)
		}
	}
	return nil
}
