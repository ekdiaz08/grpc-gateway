# Grpc Gateway

go to third_party/googleapis folder

```
   cd ~/go/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis
```

copy package google to your project (proto)
```
    cp -r google ~/platform/grpc-gateway/proto 
```
