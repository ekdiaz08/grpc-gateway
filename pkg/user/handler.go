package user

import (
	"context"
	"fmt"
)

type Handler struct {
	UnimplementedUserServiceServer
}

func NewHandler() UserServiceServer {
	return &Handler{}
}

func (h *Handler) Post(ctx context.Context, req *User) (*User, error) {
	return &User{
		Message: fmt.Sprintf("hello %s", req.Message),
	}, nil
}

func (h *Handler) Get(ctx context.Context, req *UserRequest) (*User, error) {
	req.Name = "1ernesto2"
	return &User{
		Message: fmt.Sprintf("hello %s", "ernesto"),
	}, nil
}
