#generate contract

gen: 
	protoc -I ./proto \
  	--go_out ./proto \
	--go_opt paths=source_relative \
  	--go-grpc_out ./proto \
	--go-grpc_opt paths=source_relative \
  	--grpc-gateway_out ./proto --grpc-gateway_opt paths=source_relative \
	--openapiv2_out . \
	--openapiv2_opt logtostderr=true \
	--openapiv2_opt generate_unbound_methods=true \
  	./proto/*.proto

proxy:
	go run cmd/proxy/main.go

server:
	go run cmd/server/main.go

api:
	go run cmd/api/main.go