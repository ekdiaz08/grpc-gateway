package main

import (
	"log"
	"os"

	"gitlab.com/ekdiaz08/grpc-gateway/internal/proxy"
	"gitlab.com/ekdiaz08/grpc-gateway/internal/server"
)

func main() {
	go func() {
		if err := proxy.StartHttp(); err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
	}()

	if err := server.StartGrpc(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
